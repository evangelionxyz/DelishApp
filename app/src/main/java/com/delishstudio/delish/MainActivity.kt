package com.delishstudio.delish

import android.content.pm.PackageManager
import android.graphics.Rect
import android.os.Build
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.delishstudio.delish.databinding.ActivityMainBinding
import com.delishstudio.delish.fragments.HomeFragment
import com.delishstudio.delish.fragments.MysteryBoxFragment
import com.delishstudio.delish.fragments.ProfileFragment
import com.delishstudio.delish.fragments.TransactionFragment
import com.delishstudio.delish.system.ActivityUtils
import nl.joery.animatedbottombar.AnimatedBottomBar
import android.Manifest


class MainActivity : AppCompatActivity() {
    private lateinit var mBinding: ActivityMainBinding
    private lateinit var mBottomNavigationBar: AnimatedBottomBar

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        supportActionBar?.hide()

        mBinding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(mBinding.root)
        bottomNavigation()

        if (savedInstanceState == null) {
            ActivityUtils.navigateToFragment(this, HomeFragment())
        }

        requestNotificationPermission()
    }

    private fun bottomNavigation() {
        mBottomNavigationBar = mBinding.bottomNavigation

        mBottomNavigationBar.setOnTabSelectListener(object : AnimatedBottomBar.OnTabSelectListener {
            override fun onTabSelected(lastIndex: Int, lastTab: AnimatedBottomBar.Tab?, newIndex: Int, newTab: AnimatedBottomBar.Tab) {
                when (newTab.id) {
                    R.id.navigation_home -> {
                        ActivityUtils.navigateToFragment(this@MainActivity, HomeFragment())
                    }
                    R.id.navigation_mystery_box -> {
                        ActivityUtils.navigateToFragment(this@MainActivity, MysteryBoxFragment())
                    }
                    R.id.navigation_transactions -> {
                        ActivityUtils.navigateToFragment(this@MainActivity, TransactionFragment())
                    }
                    R.id.navigation_userprofile -> {
                        ActivityUtils.navigateToFragment(this@MainActivity, ProfileFragment())
                    }
                }
            }
        })

        mBinding.root.viewTreeObserver.addOnGlobalLayoutListener {
            if (isKeyboardShown(mBinding.root.rootView)) {
                mBinding.bottomNavigation.visibility = View.GONE
            } else {
                mBinding.bottomNavigation.visibility = View.VISIBLE
            }
        }
    }

    private fun isKeyboardShown(rootView: View): Boolean {
        val rect = Rect()
        rootView.getWindowVisibleDisplayFrame(rect)
        val screenHeight = rootView.height
        val keypadHeight = screenHeight - rect.bottom
        return keypadHeight > screenHeight * 0.10
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == 101) {
            if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Toast.makeText(this, "Notification permission granted", Toast.LENGTH_SHORT).show()
            } else {
                Toast.makeText(this, "Notification permission denied", Toast.LENGTH_SHORT).show()
            }
        }
    }

    private fun requestNotificationPermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU) {
            if (ContextCompat.checkSelfPermission(
                    this,
                    Manifest.permission.POST_NOTIFICATIONS
                ) != PackageManager.PERMISSION_GRANTED
            ) {
                ActivityCompat.requestPermissions(
                    this,
                    arrayOf<String>(Manifest.permission.POST_NOTIFICATIONS), 101
                )
            }
        }
    }

    override fun onBackPressed() {
        super.onDestroy()
    }
}