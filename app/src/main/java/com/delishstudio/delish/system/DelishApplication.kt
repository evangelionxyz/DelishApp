package com.delishstudio.delish.system

import android.app.Application
import android.app.NotificationChannel
import android.app.NotificationManager

class DelishApplication : Application() {
    override fun onCreate() {
        super.onCreate()
        createNotificationChannel()
    }

    private fun createNotificationChannel() {
        val channel = NotificationChannel(
            NotificationService.CHANNEL_ID,
            "Pesanan", NotificationManager.IMPORTANCE_DEFAULT
        )

        channel.description = "Pesanan Makanan"

        val manager = getSystemService(
            NotificationManager::class.java
        )
        manager.createNotificationChannel(channel)
    }
}