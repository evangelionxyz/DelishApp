package com.delishstudio.delish.system

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent

class NotificationReceiver : BroadcastReceiver() {
    override fun onReceive(context: Context, intent: Intent) {
        val service = NotificationService(context)

        val title = intent.getStringExtra("title");
        val message = intent.getStringExtra("message");

        service.showNotification(title, message)
    }
}