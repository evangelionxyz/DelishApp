package com.delishstudio.delish.activities.adapters

import android.content.Intent
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.TextView
import androidx.appcompat.widget.AppCompatButton
import androidx.recyclerview.widget.AsyncListDiffer
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.delishstudio.delish.model.FoodModel
import com.delishstudio.delish.activities.checkout.CheckoutActivity
import com.delishstudio.delish.databinding.RcDiscountItemListBinding
import com.delishstudio.delish.model.UserManager
import com.delishstudio.delish.system.Utils
import com.delishstudio.delish.viewmodel.DiscountItemListViewModel.Companion.foodList

class DiscountItemListAdapter: RecyclerView.Adapter<DiscountItemListAdapter.ViewHolder>(){

    inner class ViewHolder(val mBinding: RcDiscountItemListBinding) : RecyclerView.ViewHolder(mBinding.root) {
        var name: TextView = mBinding.txtFoodListFoodName
        var quantity: TextView = mBinding.txtFoodListAvailQuantity
        var category: TextView = mBinding.txtFoodListCategory
        var price: TextView = mBinding.txtFoodListPrice
        private var buyButton: AppCompatButton = mBinding.btFoodListBuy

        init {
            buyButton.setOnClickListener{
                val currentFood = itemDiffer.currentList[bindingAdapterPosition]

                if(UserManager.Main.addressList.isNotEmpty()) {

                    for (i in 0 until UserManager.Main.transaction.foodList.size) {
                        if (UserManager.Main.transaction.foodList[i].id == currentFood.id) {
                            UserManager.Main.transaction.foodList.removeAt(i)
                            currentFood.buyQuantity = 0
                            break
                        }
                    }

                    val food = foodList[bindingAdapterPosition]
                    val shop = Utils.getShop(food.shopId)

                    val trs = UserManager.Main.transaction
                    if (trs.foodList.isNotEmpty()) {
                        val fShop = Utils.getShop(trs.foodList[0].shopId)
                        if (fShop!!.id != shop!!.id) {
                            UserManager.Main.transaction.foodList.clear()
                            Utils.LongToastText(itemView.context, "Order sebelumnya dihapus")
                        }
                    }

                    if (UserManager.Main.mainAddressIndex < 0) {
                        Utils.LongToastText(itemView.context,
                            "Pilih alamat terlebih dahulu")
                    }
                    else {

                        if (UserManager.Main.transaction.foodList.isNotEmpty()) {
                            if (!UserManager.Main.transaction.foodList[0].discount) {
                                UserManager.Main.transaction.foodList.clear()
                            }
                        }

                        currentFood.buyQuantity++
                        UserManager.Main.transaction.foodList.add(currentFood)
                        UserManager.Main.transaction.calcOrderdFoodCost()

                        val intent = Intent(itemView.context, CheckoutActivity::class.java)
                        itemView.context.startActivity(intent)
                    }
                } else {
                    Utils.LongToastText(itemView.context,
                        "Tambahkan alamat untuk memulai belanja")
                }
            }
        }
    }

    private val itemDifferCallback = object: DiffUtil.ItemCallback<FoodModel>(){
        override fun areItemsTheSame(oldItem: FoodModel, newItem: FoodModel): Boolean {
            return oldItem == newItem
        }

        override fun areContentsTheSame(oldItem: FoodModel, newItem: FoodModel): Boolean {
            return oldItem == newItem
        }

    }

    val itemDiffer = AsyncListDiffer(this, itemDifferCallback)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding = RcDiscountItemListBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = itemDiffer.currentList[position]
        with(holder) {
            mBinding.apply {
                name.text = item.name
                price.text = Utils.idFormatedCurrency(item.price)
                quantity.text = "${item.quantity} ${item.quaUnit}"
                category.text = item.getCategoryString()
            }
        }
    }

    override fun getItemCount(): Int = itemDiffer.currentList.size
}