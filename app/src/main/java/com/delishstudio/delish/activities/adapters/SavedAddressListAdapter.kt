package com.delishstudio.delish.activities.adapters

import android.content.Intent
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView
import com.delishstudio.delish.R
import com.delishstudio.delish.activities.SetupShop
import com.delishstudio.delish.activities.profile.SetupAddressActivity
import com.delishstudio.delish.databinding.RcSavedAddressListBinding
import com.delishstudio.delish.model.AddressModel
import com.delishstudio.delish.model.UserManager
import com.delishstudio.delish.system.DatabaseStrRef
import com.delishstudio.delish.system.Utils
import com.google.firebase.database.FirebaseDatabase

class SavedAddressListAdapter(
    private var addressList: ArrayList<AddressModel>): RecyclerView.Adapter<SavedAddressListAdapter.AddressHolder>() {

    private var selectedAddress: AddressModel? = null

    inner class AddressHolder(binding: RcSavedAddressListBinding): RecyclerView.ViewHolder(binding.root) {
        val label: TextView = binding.txtAddressLabel
        val recName: TextView = binding.txtReceipentName
        val phone: TextView = binding.txtReceipentPhone
        val completeAddress: TextView = binding.txtCompleteAddress
        val btEdit: CardView = binding.btEdit
        val btTrash: ImageView = binding.btTrash
        val mainFrame: LinearLayout = binding.mainFrame

        init {

            val addressListRef = FirebaseDatabase.getInstance()
                .getReference("${DatabaseStrRef.USERS}/${UserManager.Main.userId}/AddressList")
            val userRef = FirebaseDatabase.getInstance()
                .getReference("${DatabaseStrRef.USERS}/${UserManager.Main.userId}")

            btTrash.setOnClickListener {
                val id = addressList[bindingAdapterPosition].id
                addressList.removeAt(bindingAdapterPosition)

                addressListRef.child(id!!).removeValue()
                UserManager.Main.mainAddressIndex = if (addressList.isEmpty()) -1 else 0

                if(UserManager.Main.mainAddressIndex >= 0) {
                    UserManager.Main.latitude = addressList[UserManager.Main.mainAddressIndex].latitude
                    UserManager.Main.longitude = addressList[UserManager.Main.mainAddressIndex].longitude
                } else {
                    UserManager.Main.latitude = 0.0
                    UserManager.Main.longitude = 0.0
                }

                userRef.child("latitude").setValue(UserManager.Main.latitude)
                userRef.child("longitude").setValue(UserManager.Main.longitude)
                userRef.child("mainAddressIndex").setValue(UserManager.Main.mainAddressIndex)

                for (shop in SetupShop.shops) {
                    shop.updateDistance(UserManager.Main.latitude,
                        UserManager.Main.longitude)
                }

                notifyItemRemoved(bindingAdapterPosition)
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AddressHolder {
        val binding = RcSavedAddressListBinding
            .inflate(LayoutInflater.from(parent.context), parent, false)
        return AddressHolder(binding)
    }

    override fun getItemCount(): Int {
        return addressList.size
    }

    override fun onBindViewHolder(holder: AddressHolder, position: Int) {
        val address = addressList[position]

        holder.label.text = address.label
        holder.recName.text = address.receipentName
        holder.phone.text = address.receipentPhone
        holder.completeAddress.text = address.completeAddress

        val userRef = FirebaseDatabase.getInstance()
            .getReference("${DatabaseStrRef.USERS}/${UserManager.Main.userId}")

        holder.itemView.setOnClickListener {
            selectedAddress = address
            notifyDataSetChanged()
        }

        val isSelected = position == UserManager.Main.mainAddressIndex
        if (isSelected) {
            holder.mainFrame.setBackgroundResource(R.drawable.bg_rd_light_green_with_stroke)
        } else {
            holder.mainFrame.setBackgroundResource(R.drawable.bg_rd_white_with_stroke)
        }

        if(selectedAddress == address) {
            UserManager.Main.mainAddressIndex = position
            UserManager.Main.latitude = addressList.get(position).latitude
            UserManager.Main.longitude = addressList.get(position).longitude

            userRef.child("mainAddressIndex").setValue(UserManager.Main.mainAddressIndex)
            userRef.child("latitude").setValue(UserManager.Main.latitude)
            userRef.child("longitude").setValue(UserManager.Main.longitude)

            for (shop in SetupShop.shops) {
                shop.updateDistance(UserManager.Main.latitude,
                    UserManager.Main.longitude)
            }
        }

        holder.btEdit.setOnClickListener {
            val intent = Intent(holder.itemView.context, SetupAddressActivity::class.java)
            intent.putExtra("editIdx", position)
            holder.itemView.context.startActivity(intent)
        }

    }
}